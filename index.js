const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoute = require("../routes/userRoute.js")

const app = express();

//DB Connection
mongoose.connect("mongodb+srv://admin:admin123@batch218-coursebooking.bac1a6i.mongodb.net/courseBooking?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

//Prompts message in the terminal once connecton is open
mongoose.connection.once("open", () => console.log("Now connected to Ulep MongoDB Atlas"))

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use("/users", userRoute);

app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${process.env.PORT || 4000}`);
