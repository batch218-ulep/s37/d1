const express = require("express");
const router = express.Router();
const User = require("../models/user.js");
const userController = require("../controllers/userController.js");

//routes folder - is where all http method and endpoints are located

router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(result => res.send(result));
})

router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(result => res.send(result));
})

router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(result => res.send(result));
})

//S38 Actvity - Code Along
router.post("/details", (req, res) => {
	userController.getProfile({userId : req.body.id}).then(result => res.send(result));
})

module.exports = router;
